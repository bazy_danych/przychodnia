-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 05 Wrz 2020, 12:03
-- Wersja serwera: 10.4.11-MariaDB
-- Wersja PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `przychodnia`
--
CREATE DATABASE IF NOT EXISTS `przychodnia` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `przychodnia`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `grafki`
--

DROP TABLE IF EXISTS `grafki`;
CREATE TABLE `grafki` (
  `idgrafik` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(20) NOT NULL,
  `czas_trwania` date NOT NULL,
  `aktywny` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `lekarze`
--

DROP TABLE IF EXISTS `lekarze`;
CREATE TABLE `lekarze` (
  `idkekarz` bigint(20) UNSIGNED NOT NULL,
  `idosoba` bigint(20) UNSIGNED NOT NULL,
  `idspecjalizacja` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `lekarze`
--

INSERT INTO `lekarze` (`idkekarz`, `idosoba`, `idspecjalizacja`) VALUES
(1, 5, 4),
(2, 2, 2),
(3, 6, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `lekarze_grafiki`
--

DROP TABLE IF EXISTS `lekarze_grafiki`;
CREATE TABLE `lekarze_grafiki` (
  `idlekarze_grafiki` bigint(20) UNSIGNED NOT NULL,
  `id_grafik` bigint(20) UNSIGNED NOT NULL,
  `idlekarz` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `osoby`
--

DROP TABLE IF EXISTS `osoby`;
CREATE TABLE `osoby` (
  `idosoba` bigint(20) UNSIGNED NOT NULL,
  `imie` varchar(30) NOT NULL,
  `nazwisko` varchar(50) NOT NULL,
  `pesel` char(11) NOT NULL,
  `telefom` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `osoby`
--

INSERT INTO `osoby` (`idosoba`, `imie`, `nazwisko`, `pesel`, `telefom`, `email`) VALUES
(2, 'Jak', 'Nowak', '88888888888', '000000000', ''),
(3, 'Antonina', 'Olejniczak', '99999999999', '', ''),
(4, 'Izabela', 'Adamowicz', '77777777777', '', ''),
(5, 'Bartosz', 'Olejniczak', '99999999999', '', ''),
(6, 'Lucjan', 'Olejniczak', '99999999999', '', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przyjecia`
--

DROP TABLE IF EXISTS `przyjecia`;
CREATE TABLE `przyjecia` (
  `idprzyjecie` bigint(20) UNSIGNED NOT NULL,
  `idlekarze_grafiki` bigint(20) UNSIGNED NOT NULL,
  `dzien` set('poniedziałek','wotrek','środa','czwartek','piątek','sobota') NOT NULL,
  `godzina_start` time NOT NULL,
  `godzina_stop` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `specjalizacje`
--

DROP TABLE IF EXISTS `specjalizacje`;
CREATE TABLE `specjalizacje` (
  `idspecjalizacja` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(60) NOT NULL,
  `opis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `specjalizacje`
--

INSERT INTO `specjalizacje` (`idspecjalizacja`, `nazwa`, `opis`) VALUES
(1, 'internista', ''),
(2, 'pediatra', ''),
(3, 'laryngolog', ''),
(4, 'dermatolog', ''),
(5, 'dentysta', ''),
(6, 'stomatolog', '');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `grafki`
--
ALTER TABLE `grafki`
  ADD UNIQUE KEY `idgrafik` (`idgrafik`);

--
-- Indeksy dla tabeli `lekarze`
--
ALTER TABLE `lekarze`
  ADD UNIQUE KEY `idkekarz` (`idkekarz`),
  ADD KEY `idosoba` (`idosoba`),
  ADD KEY `idspecjalizacja` (`idspecjalizacja`);

--
-- Indeksy dla tabeli `lekarze_grafiki`
--
ALTER TABLE `lekarze_grafiki`
  ADD UNIQUE KEY `idlekarze_grafiki` (`idlekarze_grafiki`),
  ADD KEY `idlekarz` (`idlekarz`),
  ADD KEY `id_grafik` (`id_grafik`);

--
-- Indeksy dla tabeli `osoby`
--
ALTER TABLE `osoby`
  ADD UNIQUE KEY `idosoba` (`idosoba`);

--
-- Indeksy dla tabeli `przyjecia`
--
ALTER TABLE `przyjecia`
  ADD UNIQUE KEY `idprzyjecie` (`idprzyjecie`),
  ADD KEY `idlekarze_grafiki` (`idlekarze_grafiki`);

--
-- Indeksy dla tabeli `specjalizacje`
--
ALTER TABLE `specjalizacje`
  ADD UNIQUE KEY `idspecjalizacja` (`idspecjalizacja`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `grafki`
--
ALTER TABLE `grafki`
  MODIFY `idgrafik` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `lekarze`
--
ALTER TABLE `lekarze`
  MODIFY `idkekarz` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `lekarze_grafiki`
--
ALTER TABLE `lekarze_grafiki`
  MODIFY `idlekarze_grafiki` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `osoby`
--
ALTER TABLE `osoby`
  MODIFY `idosoba` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `przyjecia`
--
ALTER TABLE `przyjecia`
  MODIFY `idprzyjecie` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `specjalizacje`
--
ALTER TABLE `specjalizacje`
  MODIFY `idspecjalizacja` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `lekarze`
--
ALTER TABLE `lekarze`
  ADD CONSTRAINT `lekarze_ibfk_1` FOREIGN KEY (`idosoba`) REFERENCES `osoby` (`idosoba`),
  ADD CONSTRAINT `lekarze_ibfk_2` FOREIGN KEY (`idspecjalizacja`) REFERENCES `specjalizacje` (`idspecjalizacja`);

--
-- Ograniczenia dla tabeli `lekarze_grafiki`
--
ALTER TABLE `lekarze_grafiki`
  ADD CONSTRAINT `lekarze_grafiki_ibfk_1` FOREIGN KEY (`idlekarz`) REFERENCES `lekarze` (`idkekarz`),
  ADD CONSTRAINT `lekarze_grafiki_ibfk_2` FOREIGN KEY (`id_grafik`) REFERENCES `grafki` (`idgrafik`);

--
-- Ograniczenia dla tabeli `przyjecia`
--
ALTER TABLE `przyjecia`
  ADD CONSTRAINT `przyjecia_ibfk_1` FOREIGN KEY (`idlekarze_grafiki`) REFERENCES `lekarze_grafiki` (`idlekarze_grafiki`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
